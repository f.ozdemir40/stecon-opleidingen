from .base import *

# Enable Debugging messages
DEBUG = False

# The URL prefix that is used for accessing static files.
STATIC_URL = "/static/"

# The location where the static assets of the collectstatic command will be stored.
STATIC_ROOT = os.path.join(BASE_DIR, "static")