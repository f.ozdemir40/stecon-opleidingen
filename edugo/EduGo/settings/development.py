from .base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['*']

# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

STATIC_URL = "/static/"

# The location where the static assets of the collectstatic command will be stored.
STATIC_ROOT = os.path.join(BASE_DIR, "static")

# Additional locations to look for static content when running collectstatic.
# When in DEBUG mode these directories will also be searched for static content.
STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "frontend/static"),
]