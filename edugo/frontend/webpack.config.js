const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const bourbon = require('node-bourbon');
const neat = require('node-neat');
const path = require('path');

/* global process, __dirname */

// Extract the style sheets into a dedicated file for production.


module.exports = {
    mode: 'development',
    context: path.resolve(__dirname, 'src'),
    entry: './main.js',
    output: {
        filename: 'js/main.js',
        path: path.resolve(__dirname, 'static'),
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: '[name].css',
        }),
    ],
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: [
                    // Transcompile ES2015
                    'babel-loader',
                    // Linter must come before any other loader
                    'eslint-loader',
                ],
            },
            {
                test: /\.scss$/,
                use: [
                    {
                    loader: MiniCssExtractPlugin.loader,
                    options: {
                        
                        hmr: process.env.NODE_ENV === 'development',
                    },
                },
                'css-loader',
                {
                    loader: 'sass-loader',
                    options: {
                        includePaths: [
                            bourbon.includePaths,
                            neat.includePaths,
                        ],
                    }
                }
                ],
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                use: [
                    'file-loader',
                ],
            },
        ],
    },
    devServer: {
        contentBase: path.resolve(__dirname, 'src'),
    },
};
